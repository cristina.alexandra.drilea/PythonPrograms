import zipfile
import re
import os

# with zipfile.ZipFile('unzip_me_for_instructions.zip', 'r') as zip_ref:
#    zip_ref.extractall('extracted_here')

pattern = r'\d{3}-\d{3}-\d{4}'


def search_for_regex(file, p=pattern):
    f = open(file, 'r')
    text = f.read()

    if re.search(p, text):
        return re.search(p, text)
    else:
        return ''


results = []
variable = os.getcwd() + '/extracted_here/extracted_content'
print(variable)

for folder, sub_folder, files in os.walk(os.getcwd() + '/extracted_here/extracted_content'):

    for f in files:
        full_path = folder + '/' + f
        results.append(search_for_regex(full_path))

for r in results:
    if r != '':
        print(r.group())
