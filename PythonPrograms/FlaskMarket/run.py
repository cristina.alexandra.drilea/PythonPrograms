from market import app

# Checks if the ru.py has executed directly and is not imported
if __name__ == '__main__':
    app.run(debug=True)
